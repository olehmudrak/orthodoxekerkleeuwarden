<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\ServiceType;

class ServiceTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ServiceType::create([
            'name' => 'Vesper',
            'time' => '16:00',
        ]);

        ServiceType::create([
            'name' => 'H.Liturgie',
            'time' => '10:30',
        ]);
    }
}
