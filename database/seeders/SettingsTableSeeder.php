<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SettingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('settings')->insert([
            'title' => 'Orthodoxe Kerk Leeuwarden',
            'description' => 'Website van de Russisch Orthodox Kerk te Leeuwarden (Frl.).',
            'keywords' => 'russisch,orthodox,orthodoxe,kerk,diensten,leeuwarden,friesland,geloof,religie,icoon',
            'author' => 'Oleh Mudrak',
            'location' => 'De Koepel Bilgaard',
            'adres' => 'De Hooidollen 8',
            'postal_code' => '8918 HV',
            'city' => 'Leeuwarden',
            'name' => 'vader Jewsewy',
            'phone_number' => '0514-581537',
            'email' => 'info@kloosterhemelum.nl'
        ]);
    }
}
