<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PagesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('pages')->insert([
            [
                'title' => 'Welkom',
                'subtitle' => 'Russisch Orthodoxe kerk Leeuwarden van Heilige Johannes de Doper Patriachaat van Moskou',
                'banner' => 'empty.jpg',
                'content' => '[{"title":"Kerk","content":"Sinds 2009 worden er vanuit het klooster diensten georganiseerd in Leeuwarden. Voorheen in de Stelp aan de Huizumerlaan. Vanaf zondag 8 december 2019 zullen de diensten in Leeuwarden plaatsvinden in het gebouw De Koepel in de wijk Bilgaard, Hooidollen 8, 8918 HV Leeuwarden.) Zoals gebruikelijk zal ook daar na afloop van de diensten koffie en thee zijn en gelegenheid om met elkaar kennis te maken of vragen te stellen.","image":"empty.jpg"},{"title":"Informatie","content":"Ut nunc nisl, lobortis non tincidunt quis, accumsan et ante. Pellentesque faucibus, nisi nec dignissim gravida, arcu ante blandit nulla, ut gravida turpis velit vitae justo. Nulla malesuada tortor blandit, efficitur ante vitae, imperdiet sapien.","image":"empty.jpg"}]',
            ],
            [
                'title' => 'Diensten',
                'subtitle' => '',
                'banner' => 'empty.jpg',
                'content' => '[{"title":"","content":"","image":""},{"title":"","content":"","image":""}]',
            ],
            [
                'title' => 'Bijzonder verzoek',
                'subtitle' => '',
                'banner' => 'empty.jpg',
                'content' => '[{"title":"","content":"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce sapien augue, viverra ut fermentum vel, aliquet ut arcu. Donec porttitor egestas dolor et feugiat. Nulla facilisi. Pellentesque in iaculis elit. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos","image":""}]',
            ],
            [
                'title' => 'Galerij',
                'subtitle' => '',
                'banner' => 'empty.jpg',
                'content' => '[{"title":"","content":"","image":""}]',
            ],
            [
                'title' => 'Contact',
                'subtitle' => '',
                'banner' => 'empty.jpg',
                'content' => '[{"title":"","content":"","image":""}]',
            ],
            [
                'title' => 'Links',
                'subtitle' => '',
                'banner' => 'empty.jpg',
                'content' => '[{"title":"","content":"","image":""}]',
            ],
            [
                'title' => 'Russische Orthodoxe kerk patriachaat van Moskou',
                'subtitle' => '',
                'banner' => 'empty.jpg',
                'content' => '[{"title":"Aartsbisshop van Den Haag en Nederland","content":"Vladiko Elisey (Ganaba)","image":"empty.jpg"},{"title":"Priester van onze Kerk","content":"Hieromonk Jewsewy","image":"empty.jpg"},{"title":"","content":"<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce sapien augue, viverra ut fermentum vel, aliquet ut arcu. Donec porttitor egestas dolor et feugiat. Nulla facilisi. Pellentesque in iaculis elit. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos<\/p>","image":""}]',
            ]
        ]);
    }
}
