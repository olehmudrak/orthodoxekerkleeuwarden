@extends('layouts.app')

@section('content')
<div class="position-relative overflow-hidden text-center bg-light background-image partial-height mb-3"  
        style="background-image: linear-gradient(rgba(0, 0, 0, 0.5),rgba(0, 0, 0, 0.5)), url('{{ asset('storage/images/' . $page->banner) }}')">    
    <div class="container top-container middle">
        <div class="py-5">
            <h1 class="display-3 text-white font-weight-bold">{{ $page->title }}</h1>
        </div>
    </div>
</div>
<div class="container">
    <div class="col-12 text-center ">
        <p>
            {!! $content[0]->content !!}
        </p>
    </div>
</div>
@if(isset($page->category) && count($page->category->images) > 0)
    <div class="container justify-content-center pb-5 pt-2 mb-5">
        <div class="row">
            <div class="col-12">
                <div id="carouselExampleIndicators" class="carousel slide mx-auto d-block" data-ride="carousel">
                    <ol class="carousel-indicators">
                        @foreach ($page->category->images as $image)
                            <li data-target="#carouselExampleIndicators" data-slide-to="{{ $image->id }}"{{ $image === $page->category->images[0] ? ' class="active"' : '' }}></li>
                        @endforeach
                    </ol>
                    <div class="carousel-inner">
                        @foreach ($page->category->images as $image)
                        <div class="carousel-item{{ $image === $page->category->images[0] ? ' active' : '' }}">
                            <img class="d-block w-100" src="{{ asset('storage/images/' . $image->image) }}" alt="{{ $image->title }}">
                        </div>
                        @endforeach
                    </div>
                    <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>
            </div>
            <div class="col-12 text-center pt-2">
                <a href="{{ route('gallery.index')}}"><button class="btn btn-primary">Meer foto's</button></a>
            </div>
        </div>
    </div>
@endif

@include('partials.footer')
@endsection
