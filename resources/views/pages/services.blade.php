@extends('layouts.app')

@section('content')
<div class="position-relative overflow-hidden text-center bg-light background-image partial-height mb-3"
    style="background-image: linear-gradient(rgba(0, 0, 0, 0.5),rgba(0, 0, 0, 0.5)), url({{ asset('storage/images/' . $page->banner) }})">
    <div class="container top-container middle">
        <div class="py-5">
            <h1 class="display-3 text-white font-weight-bold">{{ $page->title }}</h1>
        </div>
    </div>
</div>
@if (!empty($types))
<div class="container">
    <div class="col-12 text-center block">
        <h2 class="text-primary font-weight-bold">Tijden</h2>
    </div>
</div>

<div class="container mb-3">
    <ul class="list-group text-center">
        @foreach ($types as $type)
        <li class="list-group-item bg-light text-primary">{{ $type->name }} : {{ $type->time }} {{ $type->description }}
        </li>
        @endforeach
    </ul>
</div>
@endif
@if (!empty($services))
<div class="container">
    <div class="col-12 text-center block">
        <h2 class="text-primary font-weight-bold">Datums</h2>
    </div>
</div>
<div class="container mb-3">
    <ul class="list-group text-center">
        @foreach ($services as $service)
        <li class="list-group-item bg-light text-primary">{{ \Carbon\Carbon::parse($service->date)->translatedFormat("d
            F Y") }} - {{ $service->type->time }} {{ $service->type->name }} {{ $service->description }}</li>
        @endforeach
    </ul>
</div>
@endif
@include('partials.footer')
@endsection