@extends('layouts.app')

@section('content')
<div class="container-fluid mb-3 bg-brown">
    <div class="container">
        <div class="row justify-content-between">
            <div class="col-md-4 col-sm-12">
                <p class="text-center text-white description">{{ $content[0]->title }}</p>
                <img src="{{ asset('storage/images/' . $content[0]->image) }}" class="photo person rounded mx-auto d-block" alt="Aartsbisshop Elisey Ganaba">
                <h3 class="text-center text-white mt-2">{{ $content[0]->content }}</h3>
            </div>
            <div class="col-md-4 col-sm-12">
                <h3 class="text-white text-center description">
                    {{ $page->title }}
                </h3>
                <p class="text-white text-center description"></p>
                <img class="photo description small mx-auto d-block no-shadow" src="{{ asset('storage/images/' . $content[2]->image ?? $page->banner) }}" alt="">

            </div>
            <div class="col-md-4 col-sm-12">
                <p class="text-center text-white description">{{ $content[1]->title }}</p>
                <img src="{{ asset('storage/images/' . $content[1]->image) }}" class="photo person rounded mx-auto d-block" alt="Hieromonk Jevsewy">
                <h3 class="text-center text-white mt-2">{{ $content[1]->content }}</h3>
            </div>
        </div>
    </div>
</div>
<div class="container">
    <div class="col-12 text-center ">
        {!! $content[2]->content !!}
    </div>
</div>

@include('partials.footer')
@endsection

