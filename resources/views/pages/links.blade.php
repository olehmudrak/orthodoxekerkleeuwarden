@extends('layouts.app')

@section('content')
<div class="position-relative overflow-hidden text-center bg-light background-image partial-height mb-3"  
        style="background-image: linear-gradient(rgba(0, 0, 0, 0.5),rgba(0, 0, 0, 0.5)), url({{ asset('storage/images/' . $page->banner) }})">    
    <div class="container top-container middle">
        <div class="py-5">
            <h1 class="display-3 text-white font-weight-bold">{{ $page->title }}</h1>
        </div>
    </div>
</div>
<div class="container mb-3">
    <div class="col-12">
        <ul class="list-group text-center">
            @foreach ($links as $link)
                <a href="{{ $link->url }}" target="_blank"><li class="list-group-item block text-white">{{ $link->title }}</li></a>
            @endforeach
        </ul>
    </div>
</div>

@include('partials.footer')
@endsection
