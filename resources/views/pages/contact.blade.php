@extends('layouts.app')

@section('content')
<div class="position-relative overflow-hidden text-center bg-light background-image partial-height mb-3"  
        style="background-image: linear-gradient(rgba(0, 0, 0, 0.5),rgba(0, 0, 0, 0.5)), url({{ asset('storage/images/' . $page->banner) }})">    
    <div class="container top-container middle">
        <div class="py-5">
            <h1 class="display-3 text-white font-weight-bold">{{ $page->title }}</h1>
        </div>
    </div>
</div>
<div class="container mb-3">
    
    @include('partials.backend.flash-message')

    <div class="col-12 text-center">
        <h4>
            {{ $setting->name }} <br><br>
            {{ $setting->phone_number }} <br>
            {{ $setting->email }} <br><br>
        </h4>
    </div>
</div>
<div class="container">
    <div class="col-12 text-center block">
        <h2 class="text-primary font-weight-bold">Formulier</h2>
    </div>
</div>
<div class="container mb-5 pb-3">
    <div class="col-12">
        <form action="{{ route('mail.contact') }}" method="POST">
            @csrf

            <div class="form-group">
                <label for="name">Naam</label>
                <input type="text" class="form-control" id="name" name="name" placeholder="Voer in naam" value="{!! old('name') !!}">
                @if($errors->has('name'))
                    <div class="alert alert-danger" role="alert">{{ $errors->first('name') }}</div>
                @endif
            </div>
            <div class="form-group">
                <label for="email">Email</label>
                <input type="text" class="form-control" id="email" name="email" placeholder="Voer in email" value="{!! old('email') !!}">
                @if($errors->has('email'))
                    <div class="alert alert-danger" role="alert">{{ $errors->first('email') }}</div>
                @endif
            </div>
            <div class="form-group">
                <label for="subject">Onderwerp</label>
                <input type="text" class="form-control" id="subject" name="subject" placeholder="Voer in onderwerp" value="{!! old('subject') !!}">
                @if($errors->has('subject'))
                    <div class="alert alert-danger" role="alert">{{ $errors->first('subject') }}</div>
                @endif
            </div>
            <div class="form-group">
                <label for="content">Inhoud</label>
                <textarea name="content" id="content" cols="30" rows="10" class="form-control" name="content" placeholder="Voer in inhoud" value="{!! old('content') !!}"></textarea>
                @if($errors->has('content'))
                    <div class="alert alert-danger" role="alert">{{ $errors->first('content') }}</div>
                @endif
              </div>
            <button type="submit" class="btn btn-primary float-right">Verzenden</button>
          </form>
    </div>
</div>

@include('partials.footer')
@endsection
