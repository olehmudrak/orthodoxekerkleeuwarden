@extends('layouts.app')

@section('content')
<div class="background-image screen-height"
    style="background-image: linear-gradient(rgba(72, 2, 2, 1),rgba(72, 2, 2, 0.8),rgba(156, 139, 100, 1)), url({{ asset('storage/images/' . $page->banner) }})">
    <div class="container top-container">
        <div class="py-5">
            <h1 class="display-3 text-white font-weight-bold">
                {{ $page->title }}
            </h1>
            <h3 class="text-white">
                {{ $page->subtitle }}
            </h3>
        </div>

    </div>
</div>
<div class="container">
    <div class="col-12 text-center">
        <h2 class="text-primary font-weight-bold">Diensten</h2>
    </div>
</div>
<div class="container mb-5">
    <div class="row">
        <div class="col-12 text-center">
            <h5>
                @foreach ($types as $type)
                <span>{{ $type->name }} : {{ $type->time }} {{ $type->description }}</span><br>
                @endforeach
            </h5>
            <h4>
                @foreach ($services as $service)
                <br><span style="{{ $service->color ? 'color:' . $service->color . ';' : '' }}">{{ \Carbon\Carbon::parse($service->date)->translatedFormat("d F Y") }} - {{
                    $service->type->time }} {{ $service->type->name }} {{
                    $service->description }}</span>
                @endforeach
            </h4>
        </div>
        <div class="col-12 text-center pt-2">
            <a href="{{ route('pages.services')}}"><button class="btn btn-primary">Volledige rooster</button></a>
        </div>
    </div>
</div>
<div class="container">
    <div class="col-12 text-center">
        <h2 class="text-primary font-weight-bold">Diensten worden gehouden op het volgende adres</h2>
    </div>
</div>
<div class="container mb-5">
    <div class="row">
        <div class="col-12 text-center">
            <h4>
                {{ $setting->location }} <br>
                {{ $setting->adres }}<br>
                {{ $setting->postal_code }} {{ $setting->city }} <br>
            </h4>
        </div>
    </div>
</div>
<div class="container">
    <div class="col-12 text-center block">
        <h2 class="text-primary font-weight-bold">
            {{ $content[0]->title }}
        </h2>
    </div>
</div>
<div class="container mb-5 pb-5 pt-2">
    <div class="row">
        <div class="col-12">
            <img class="photo description row-picture float-left"
                src="{{ asset('storage/images/' . $content[0]->image) }}" alt="">
            <p class="text-secondary">
                {!! $content[0]->content !!}
            </p>
            <a href="{{ route('pages.about')}}"><button class="btn btn-primary float-right">Meer over kerk</button></a>
        </div>
    </div>
</div>
<div class="container">
    <div class="col-12 text-center block">
        <h2 class="text-primary font-weight-bold">
            {{ $content[1]->title }}
        </h2>
    </div>
</div>
<div class="container mb-5 pb-5 pt-2">
    <div class="row">
        <div class="col-12">
            <img class="photo description row-picture float-right"
                src="{{ asset('storage/images/' . $content[1]->image) }}" alt="">
            <p class="text-secondary">
                {!! $content[1]->content !!}
                <a href="{{ route('pages.contact')}}"><button class="btn btn-primary">Neem contact</button></a>
            </p>
        </div>
    </div>
</div>
@if($gallery)
<div class="container">
    <div class="col-12 text-center block">
        <h2 class="text-primary font-weight-bold">Galerij</h2>
    </div>
</div>
<div class="container justify-content-center pb-5 pt-2 mb-5">
    <div class="row">
        <div class="col-12">
            <div id="carouselExampleIndicators" class="carousel slide mx-auto d-block" data-ride="carousel">
                <ol class="carousel-indicators">
                    @foreach ($gallery as $image)
                    <li data-target="#carouselExampleIndicators" data-slide-to="{{ $image->id }}" {{ $image==$gallery[0]
                        ? ' class="active"' : '' }}></li>
                    @endforeach
                </ol>
                <div class="carousel-inner">
                    @foreach ($gallery as $image)
                    <div class="carousel-item{{ $image === $gallery[0] ? ' active' : '' }}">
                        <img class="d-block w-100" src="{{ asset('storage/images/' . $image->image) }}"
                            alt="{{ $image->title }}">
                    </div>
                    @endforeach
                </div>
                <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>
        </div>
        <div class="col-12 text-center pt-2">
            <a href="{{ route('gallery.index')}}"><button class="btn btn-primary">Meer foto's</button></a>
        </div>
    </div>
</div>
@endif

@include('partials.footer')
@endsection