@extends('layouts.app')

@section('content')
<div class="position-relative overflow-hidden text-center bg-light background-image partial-height mb-3"  
        style="background-image: linear-gradient(rgba(0, 0, 0, 0.5),rgba(0, 0, 0, 0.5)), url({{ asset('storage/images/' . $page->banner) }})">    
    <div class="container top-container middle">
        <div class="py-5">
            <h1 class="display-3 text-white font-weight-bold">{{ $page->title }}</h1>
        </div>
    </div>
</div>
<div class="container mb-3">
    <div class="row d-flex justify-content-start no-gutters">
        @foreach ($categories as $category)
            @if(count($category->images) > 0)
                <div class="col-md-4 col-sm-6 col-xs-12 img-gallery-container py-1 px-1">
                    <div>
                        <a href="{{ route('gallery.show', $category->id) }}">
                            <img class="card-img-top" src="{{ asset('storage/images/' . $category->images[count($category->images) - 1]->image) }}" alt="{{ $category->name }}">
                            <div class="card-body block text-primary">
                                <p class="card-text text-center">{{ $category->name }}</p>
                            </div>
                        </a>
                    </div>
                </div>
            @endif
        @endforeach
    </div>
</div>

@include('partials.footer')
@endsection
