@extends('layouts.app')

@section('content')
<div class="position-relative overflow-hidden text-center bg-light background-image partial-height mb-3"  
        style="background-image: linear-gradient(rgba(0, 0, 0, 0.5),rgba(0, 0, 0, 0.5)), url('{{ asset('/storage/images/' . $category->images[count($category->images) - 1]->image) }}')">    
    <div class="container top-container middle">
        <div class="py-5">
            <h1 class="display-3 text-white font-weight-bold">{{ $category->name }}</h1>
        </div>
    </div>
</div>
<div class="container justify-content-center mb-3">
    <div class="row">
        <div class="col-12">
            <button onclick="window.history.back()" class="btn btn-primary">Terug</button>
        </div>
    </div>
</div>
<div class="container justify-content-center">
    <div class="row">
        <div class="col-12">
            <gallery-component :images="{{ json_encode($images) }}"></gallery-component>
        </div>
    </div>
</div>
@include('partials.footer')
@endsection
