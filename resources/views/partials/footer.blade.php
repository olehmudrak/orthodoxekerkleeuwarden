<footer class="page-footer font-small blue pt-4 bg-dark text-white">
    <div class="container-fluid text-center text-md-left">
        <div class="row">
            <div class="col-md-6 mt-md-0 mt-3 text-center">
                <h5 class="text-uppercase">Adres</h5>
                <p>
                    {{ $setting->location }} <br>
                    {{ $setting->adres }}<br>
                    {{ $setting->postal_code }} {{ $setting->city }} <br>
                </p>  
            </div>  
            <hr class="clearfix w-100 d-md-none pb-3">
            <div class="col-md-3 mb-md-0 mb-3">
                <ul class="list-unstyled">
                    <li>
                    <a href="{{ route('pages.index') }}">Home</a>
                    </li>
                    <li>
                        <a href="{{ route('pages.services') }}">Diensten</a>
                    </li>
                    <li>
                        <a href="{{ route('pages.request') }}">Verzoek</a>
                    </li>
                    <li>
                        <a href="{{ route('gallery.index') }}">Galerij</a>
                    </li>
                </ul>
            </div>
            <div class="col-md-3 mb-md-0 mb-3">
                <ul class="list-unstyled">
                    <li>
                        <a href="{{ route('pages.contact') }}">Contact</a>
                    </li>
                    <li>
                        <a href="{{ route('pages.links') }}">Links</a>
                    </li>
                    <li>
                        <a href="{{ route('pages.about') }}">About</a>
                    </li>
                </ul>
            </div>
        </div>  
    </div>    
    <div class="footer-copyright text-center py-3">© 2020 Copyright:
      <a href="{!! env('APP_URL', 'http://www.orthodoxekerkleeuwarden.nl') !!}"> OrthodoxeKerkLeeuwarden.nl</a>
    </div>
</footer>
