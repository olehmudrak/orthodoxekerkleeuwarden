<footer class="main-footer">
    <strong>Copyright &copy; 2020 <a href="{!! env('APP_URL', 'http://www.orthodoxekerkleeuwarden.nl') !!}"> OrthodoxeKerkLeeuwarden.nl</a> Alle rechten voorbehouden.</strong>
</footer>

