  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <div class="sidebar">
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="info">
          <a href="#" class="d-block">{{ Auth::user()->name }}</a>
        </div>
      </div>
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <li class="nav-item">
            <a href="{{ route('backend.dashboard') }}" class="nav-link {{ (strpos(Route::currentRouteName(), 'backend.dashboard') === 0) ? 'active' : '' }}">
              <i class="nav-icon fas fa-th"></i>
              <p>
                Dashboard
              </p>
            </a>
          </li>
          <li class="nav-item has-treeview">
            @php
              $links = ['home', 'services', 'request', 'gallery'];
              $currentRoute = substr(Route::currentRouteName(), 14);
            @endphp
            <a href="#" class="nav-link {{ in_array($currentRoute, $links) ? 'active' : '' }}">
              <i class="nav-icon fas fa-book"></i>
              <p>
                Pagina's
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{ route('backend.pages.home') }}" class="nav-link {{ (strpos(Route::currentRouteName(), 'backend.pages.home') === 0) ? 'active' : '' }}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Home</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{ route('backend.pages.services') }}" class="nav-link {{ (strpos(Route::currentRouteName(), 'backend.pages.services') === 0) ? 'active' : '' }}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Diensten</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{ route('backend.pages.request') }}" class="nav-link {{ (strpos(Route::currentRouteName(), 'backend.pages.request') === 0) ? 'active' : '' }}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Verzoek</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{ route('backend.pages.gallery') }}" class="nav-link {{ (strpos(Route::currentRouteName(), 'backend.pages.gallery') === 0) ? 'active' : '' }}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Galerij</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{ route('backend.pages.contact') }}" class="nav-link {{ (strpos(Route::currentRouteName(), 'backend.pages.contact') === 0) ? 'active' : '' }}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Contact</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{ route('backend.pages.links') }}" class="nav-link {{ (strpos(Route::currentRouteName(), 'backend.pages.links') === 0) ? 'active' : '' }}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Links</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{ route('backend.pages.about') }}" class="nav-link {{ (strpos(Route::currentRouteName(), 'backend.pages.about') === 0) ? 'active' : '' }}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Over</p>
                </a>
              </li>
            </ul>
          </li>
          <li class="nav-item has-treeview">
            @php
              $serviceLinks = ['backend.services.index', 'backend.services.create', 'backend.services.edit','backend.service.types.index', 'backend.service.types.create', 'backend.service.types.edit'];
            @endphp
            <a href="#" class="nav-link {{ in_array(Route::currentRouteName(), $serviceLinks) ? 'active' : '' }}">
              <i class="nav-icon fas fa-calendar-alt"></i>
              <p>
                Diensten
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{ route('backend.services.index') }}" class="nav-link {{ (strpos(Route::currentRouteName(), 'backend.services.index') === 0) ? 'active' : '' }}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Datums</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{ route('backend.service.types.index') }}" class="nav-link {{ (strpos(Route::currentRouteName(), 'backend.service.types.index') === 0) ? 'active' : '' }}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Types</p>
                </a>
              </li>
            </ul>
          </li>
          <li class="nav-item has-treeview">
            @php 
              $mediaLinks = ['backend.images.index', 'backend.images.create', 'backend.images.edit','backend.categories.index', 'backend.categories.create', 'backend.categories.edit'];
            @endphp
            <a href="#" class="nav-link {{ in_array(Route::currentRouteName(), $mediaLinks) ? 'active' : '' }}">
              <i class="nav-icon fas fa-images"></i>
              <p>
                Media
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{ route('backend.images.index') }}" class="nav-link {{ (strpos(Route::currentRouteName(), 'backend.images.index') === 0) ? 'active' : '' }}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Afbeeldingen</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{ route('backend.categories.index') }}" class="nav-link {{ (strpos(Route::currentRouteName(), 'backend.categories.index') === 0) ? 'active' : '' }}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Categorieën</p>
                </a>
              </li>
            </ul>
          </li>
          <li class="nav-item">
            <a href="{{ route('backend.links.index') }}" class="nav-link {{ (strpos(Route::currentRouteName(), 'backend.links.index') === 0) ? 'active' : '' }}">
              <i class="nav-icon fas fa-link"></i>
              <p>
                Links
              </p>
            </a>
          </li>
          @if(auth()->user()->hasRole('admin'))
          <li class="nav-item">
            <a href="{{ route('backend.settings.edit', ['setting' => 1]) }}" class="nav-link {{ (strpos(Route::currentRouteName(), 'backend.settings.index') === 0) ? 'active' : '' }}">
              <i class="nav-icon fas fa-sliders-h"></i>
              <p>
                Instellingen
              </p>
            </a>
          </li>
          <li class="nav-item">
            @php 
            $userLinks = ['backend.users.index', 'backend.users.create', 'backend.users.edit'];
            @endphp
            <a href="{{ route('backend.users.index') }}" class="nav-link {{ in_array(Route::currentRouteName(), $userLinks) ? 'active' : '' }}">
              <i class="nav-icon fas fa-users"></i>
              <p>
                Gebruikers
              </p>
            </a>
          </li>
          @endif
          <li class="nav-item">
            <a href="{{ route('backend.user.edit', ['user' => Auth::user()->id]) }}" class="nav-link {{ (strpos(Route::currentRouteName(), 'backend.user.edit') === 0) ? 'active' : '' }}">
              <i class="nav-icon fas fa-user"></i>
              <p>
                Account 
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{ route('pages.index') }}" class="nav-link">
              <i class="nav-icon fas fa-door-open"></i>
              <p>
                Home
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="{{ route('logout') }}"
                onclick="event.preventDefault();
                            document.getElementById('logout-form').submit();">
                <i class="nav-icon fas fa-sign-out-alt"></i>
                <p>Uitloggen</p>
            </a>
            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                @csrf
            </form>
          </li>
        </ul>
      </nav>
    </div>
  </aside>