@component('mail::message')
# Hallo {{ $setting->name }},
Je hebt een email ontvangen van {{ $name }}
<br>
Hier zijn de details <br>
<b>Naam:</b> {{ $name }} <br>
<b>Email:</b> {{ $email }} <br>
<b>Onderwerp:</b> {{ $subject }} <br>
<b>Inhoud:</b>
<p>{{ $content }}</p>
<br>
{{ config('app.name') }}
@endcomponent
