@extends('layouts.backend', ['title' => 'Verzoek Pagina'])

@section('content')
    <!-- Main content -->
    <div class="content">
        <div class="container">
          <div class="row">
            <div class="col-lg-12">
                <form action="{{ route('backend.pages.update', ['page' => $page]) }}" method="post" enctype="multipart/form-data">
					@csrf
					<div class="form-group">
						<label for="title">Titel</label>
						<input type="text" class="form-control" id="title" name="title" placeholder="Titel invoeren" value="{!! $page->title ?? old('title') !!}">
					</div>
					<div class="form-group">
						<label for="subtitle">Ondertitel</label>
						<input type="text" class="form-control" id="subtitle" name="subtitle" placeholder="Ondertitel invoeren" value="{!! $page->subtitle ?? old('subtitle') !!}">
					</div>
					<div class="form-group">
						<label for="banner">Banner</label>
						<input type="file" id="banner" name="banner">
					</div>
					<h4>Huidige Banner</h4>
					<a href="{{ asset('storage/images/' . $page->banner) }}">
						<img src="{{ asset('storage/images/' . $page->banner) }}" alt="Banner" class="mx-auto d-block preview-image">
					</a>
					<div class="form-group">
						<label for="category">Galerij Category</label>
						<select name="category_id" id="category" class="form-control" value="{!! $page->category->id ?? old('category_id') !!}">
							<option value="0">Geen categorie</option>
							@foreach ($categories as $category)
								<option value="{{ $category->id }}" {{ (old("category_id") == $category->id || (isset($page->category->id) && $page->category->id == $category->id) ? "selected":"") }}>{{ $category->name }}</option>
							@endforeach
						</select>
						@if($errors->has('category_id'))
							<div class="alert alert-danger" role="alert">{{ $errors->first('category_id') }}</div>
						@endif
					</div>
					<hr>
					<h4>Eerste Kolom</h4>
					<div class="form-group">
						<label for="column-title">Kolom titel</label>
						<input type="text" class="form-control" id="column-title" name="column-title[]" placeholder="Kolom titel invoeren" value="{!! $content[0]->title ?? old('column-title')[0] ?? '' !!}">
					</div>

					<div class="form-group">
						<label for="content">Inhoud</label>
						<textarea type="text" class="form-control" id="content" name="content[]" placeholder="Inhoud invoeren">{!! $content[0]->content ?? old('content') !!}</textarea>
					</div>
					<div class="form-group">
						<label for="column-image">Afbeelding</label>
						<input type="file" id="column-image" name="column-image[]">
					</div>
					<h4>Huidige Afbeelding</h4>
					<a href="{{ asset('storage/images/' . (!empty($content[0]->image) ? $content[0]->image : 'empty.jpg')) }}">
						<img src="{{ asset('storage/images/' . (!empty($content[0]->image) ? $content[0]->image : 'empty.jpg')) }}" alt="Banner" class="mx-auto d-block preview-image">
					</a>
					<hr>
                    <input type="submit" class="btn btn-primary float-right" value="Aanpassen">
				</form>
            </div>
          </div>
        </div>
      </div>
    </div>
@endsection

@section('scripts')
<script src='https://cdn.tiny.cloud/1/322cj3dr9i0pd818maccs69nl1glevlkqvvl4b4m4u463kj5/tinymce/5/tinymce.min.js' referrerpolicy="origin"></script>
<script>
  tinymce.init({
    selector: '#content',
		height : "480",
		plugins: 'link'
  });
</script>
@endsection

