@extends('layouts.backend', ['title' => 'Diensten Pagina'])

@section('content')
    <!-- Main content -->
    <div class="content">
        <div class="container">
          <div class="row">
            <div class="col-lg-12">
                <form action="{{ route('backend.pages.update', ['page' => $page]) }}" method="post" enctype="multipart/form-data">
					@csrf
					<div class="form-group">
						<label for="title">Titel</label>
						<input type="text" class="form-control" id="title" name="title" placeholder="Titel invoeren" value="{!! $page->title ?? old('title') !!}">
					</div>
					<div class="form-group">
						<label for="subtitle">Ondertitel</label>
						<input type="text" class="form-control" id="subtitle" name="subtitle" placeholder="Ondertitel invoeren" value="{!! $page->subtitle ?? old('subtitle') !!}">
					</div>
					<div class="form-group">
						<label for="banner">Banner</label>
						<input type="file" id="banner" name="banner">
					</div>
					<h4>Huidige Banner</h4>
					<a href="{{ asset('storage/images/' . $page->banner) }}">
						<img src="{{ asset('storage/images/' . $page->banner) }}" alt="Banner" class="mx-auto d-block preview-image">
					</a>
                    <input type="submit" class="btn btn-primary float-right" value="Aanpassen">
				</form>
            </div>
          </div>
        </div>
      </div>
    </div>
@endsection

