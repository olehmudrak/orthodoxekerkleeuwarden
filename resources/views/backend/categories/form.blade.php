@csrf

<div class="form-group">
    <label for="name">Naam</label>
    <input type="text" class="form-control" id="name" name="name" placeholder="Naam invoeren" value="{!! $category->name ?? old('name') !!}">
    @if($errors->has('name'))
        <div class="alert alert-danger" role="alert">{{ $errors->first('name') }}</div>
    @endif
</div>