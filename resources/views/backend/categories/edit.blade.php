@extends('layouts.backend', ['title' => 'Categorie aanpassen'])

@section('content')
	<!-- Main content -->
	<div class="content">
		<div class="container mb-3">
		  <div class="row">
        <div class="col-12">
          <button class="btn btn-secondary" onclick="window.history.back()">Terug</button>
        </div>
		  </div>
    </div>
      <div class="container">
          <form action="{{ route('backend.categories.update', ['category' => $category]) }}" method="post">
            @method('PUT')
            @include('backend.categories.form')

            <button type="submit" class="btn btn-primary float-right">aanpassen</button>
          </form>
      </div>
	  </div>
	</div>
@endsection



