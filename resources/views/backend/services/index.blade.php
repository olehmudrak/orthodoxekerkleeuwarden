@extends('layouts.backend', ['title' => 'Diensten'])

@section('content')
<!-- Main content -->
<div class="content">
	<div class="container-fluid mb-3">
		<div class="row justify-content-end">
			<a href="{{ route('backend.services.create') }}"><button
					class="btn btn-success float-right">Toevoegen</button></a>
		</div>
	</div>
	<div class="table-responsive-sm">
		<table class="table table-hover">
			<thead>
				<tr>
					<th scope="col">#</th>
					<th scope="col">Datum</th>
					<th scope="col">Type</th>
					<th scope="col">Beschrijving</th>
					<th colspan="2" scope="col">Acties</th>
				</tr>
			</thead>
			<tbody>
				@foreach ($services as $service)
				<tr>
					<th scope="row">{{ $service->id }}</th>
					<td>
						<li class="list-group-item bg-light text-primary">{{
							\Carbon\Carbon::parse($service->date)->translatedFormat("d
							F Y") }}
					</td>
					<td>{{ $service->type->name }}</td>
					<td>{{ $service->description }}</td>
					<td>
						<a href="{{ route('backend.services.edit', ['service' => $service]) }}">
							<button class="btn btn-primary"><i class="fas fa-edit mr-1"></i>Aanpassen</button>
						</a>
					</td>
					<td>
						<form action="{{ route('backend.services.destroy', ['service' => $service]) }}" method="POST">
							@method('delete')
							@csrf
							<button type="submit" class="btn btn-danger"><i
									class="fas fa-trash-alt mr-1"></i>Verwijderen</button>
						</form>
					</td>
				</tr>
				@endforeach
			</tbody>
		</table>
	</div>
	<div class="container">
		{{ $services->links() }}
	</div>
</div>
</div>
@endsection