@extends('layouts.backend', ['title' => 'Dienst aanpassen'])

@section('content')
	<!-- Main content -->
	<div class="content">
		<div class="container mb-3">
		  <div class="row">
        <div class="col-12">
          <button class="btn btn-secondary" onclick="window.history.back()">Terug</button>
        </div>
		  </div>
    </div>
      <div class="container">
          <form action="{{ route('backend.services.update', ['service' => $service]) }}" method="post">
            @method('PUT')
            @include('backend.services.form')

            <button type="submit" class="btn btn-primary float-right">aanpassen</button>
          </form>
      </div>
	  </div>
	</div>
@endsection



