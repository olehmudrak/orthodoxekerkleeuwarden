 @csrf

<div class="form-group">
    <label for="name">Naam</label>
    <input type="text" class="form-control" id="name" name="name" placeholder="Naam invoeren" value="{!! $type->name ?? old('name') !!}">
    @if($errors->has('name'))
        <div class="alert alert-danger" role="alert">{{ $errors->first('name') }}</div>
    @endif
</div>

<div class="form-group">
    <label for="time">Tijd</label>
    <time-input time="{!! $type->time ?? old('time') !!}"></time-input>
    @if($errors->has('time'))
        <div class="alert alert-danger" role="alert">{{ $errors->first('time') }}</div>
    @endif
</div>