@extends('layouts.backend', ['title' => 'Dienst types'])

@section('content')
	<!-- Main content -->
	<div class="content">
		<div class="container-fluid mb-3">
		  <div class="row justify-content-end">
			<a href="{{ route('backend.service.types.create') }}"><button class="btn btn-success float-right">Toevoegen</button></a>
		  </div>
		</div>
		<div class="table-responsive-sm">
			<table class="table table-hover">
			<thead>
				<tr>
					<th scope="col">#</th>
					<th scope="col">Naam</th>
					<th scope="col">Tijd</th>
					<th colspan="2" scope="col">Acties</th>
				</tr>
			</thead>
			<tbody>
				@foreach ($types as $type)
					<tr>
						<th scope="row">{{ $type->id }}</th>
						<td>{{ $type->name }}</td>
						<td>{{ $type->time }}</td>
						<td>
							<a href="{{ route('backend.service.types.edit', ['type' => $type]) }}">
								<button class="btn btn-primary"><i class="fas fa-edit mr-1"></i>Aanpassen</button>
							</a>
						</td>
						<td>
						<form action="{{ route('backend.service.types.destroy', ['type' => $type]) }}" method="POST">
							@method('delete')
							@csrf
							<button type="submit" class="btn btn-danger"><i class="fas fa-trash-alt mr-1"></i>Verwijderen</button>
						</form>
						</td>
					</tr>
				@endforeach
			</tbody>
			</table>
		</div>
		<div class="container">
			{{ $types->links() }}
		</div>
	  </div>
	</div>
@endsection

