@extends('layouts.backend', ['title' => 'Dienst type toevoegen'])

@section('content')
	<!-- Main content -->
	<div class="content">
		<div class="container mb-3">
		  <div class="row">
        <div class="col-12">
          <button class="btn btn-secondary" onclick="window.history.back()">Terug</button>
        </div>
		  </div>
    </div>
      <div class="container">
          <form action="{{ route('backend.service.types.store') }}" method="post">
              @include('backend.services.types.form')

              <button type="submit" class="btn btn-primary float-right">Toevoegen</button>
          </form>
      </div>
	  </div>
	</div>
@endsection



