  @csrf

<div class="form-group">
    <label for="name">Datum</label>
    <?php $service->date; ?>
    <date-input date="{!! $service->date ?? old('date') !!}"></date-input>
    @if($errors->has('date'))
        <div class="alert alert-danger" role="alert">{{ $errors->first('date') }}</div>
    @endif
</div>

<div class="form-group">
    <label for="type">Type</label>
    <select name="service_type_id" id="type" class="form-control" value="{!! $service->type->id ?? old('service_type_id') !!}">
        @foreach ($types as $type)
            <option value="{{ $type->id }}" {{ (old("service_type_id") == $type->id || (isset($service->type->id) && $service->type->id == $type->id) ? "selected":"") }}>{{ $type->name }}</option>
        @endforeach
    </select>
    @if($errors->has('service_type_id'))
        <div class="alert alert-danger" role="alert">{{ $errors->first('service_type_id') }}</div>
    @endif
</div>

<div class="form-group">
    <label for="description">Beschrijving</label>
    <textarea type="text" class="form-control" id="description" name="description" placeholder="Beschrijving invoeren">{!! $service->description ?? old('description') !!}</textarea>
    @if($errors->has('description'))
        <div class="alert alert-danger" role="alert">{{ $errors->first('description') }}</div>
    @endif
</div>

<div class="form-group">
    <label for="description">Kleur</label>
    <input type="color" class="form-control" id="color" name="color" placeholder="Tekst kleur invoeren (optioneel)" value="{{ $service->color ?? old('color') }}"/>
    @if($errors->has('color'))
        <div class="alert alert-danger" role="alert">{{ $errors->first('color') }}</div>
    @endif
</div>