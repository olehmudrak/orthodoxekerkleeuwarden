@extends('layouts.backend', ['title' => 'Dashboard'])

@section('content')
    <!-- Main content -->
    <div class="content">
        <div class="container-fluid">
          <div class="row">
            <div class="col-lg-12">
              <div class="card">
                <div class="card-body">
                  @if (session('status'))
                      <div class="alert alert-success" role="alert">
                          {{ session('status') }}
                      </div>
                  @endif

                  Je bent ingelogd!
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
@endsection

