@extends('layouts.backend', ['title' => 'Links'])

@section('content')
	<!-- Main content -->
	<div class="content">
		<div class="container-fluid mb-3">
		  <div class="row justify-content-end">
			<a href="{{ route('backend.links.create') }}"><button class="btn btn-success float-right">Toevoegen</button></a>
		  </div>
		</div>
		<div class="table-responsive-sm">
			<table class="table table-hover">
			<thead>
				<tr>
                    <th scope="col">#</th>
                    <th scope="col">Titel</th>
                    <th scope="col">Url</th>
					<th colspan="2" scope="col">Acties</th>
				</tr>
			</thead>
			<tbody>
				@foreach ($links as $link)
					<tr>
						<th scope="row">{{ $link->id }}</th>
						<td>{{ $link->title }}</td>
						<td>{{ $link->url }}</td>
						<td>
							<a href="{{ route('backend.links.edit', ['link' => $link]) }}">
								<button class="btn btn-primary"><i class="fas fa-edit mr-1"></i>Aanpassen</button>
							</a>
						</td>
						<td>
						<form action="{{ route('backend.links.destroy', ['link' => $link]) }}" method="POST">
							@method('delete')
							@csrf
							<button type="submit" class="btn btn-danger"><i class="fas fa-trash-alt mr-1"></i>Verwijderen</button>
						</form>
						</td>
					</tr>
				@endforeach
			</tbody>
			</table>
		</div>
		<div class="container">
			{{ $links->links() }}
		</div>
	  </div>
	</div>
@endsection

