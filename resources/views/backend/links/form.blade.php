@csrf

<div class="form-group">
    <label for="title">Titel</label>
    <input type="text" class="form-control" id="title" name="title" placeholder="Titel invoeren" value="{!! $link->title ?? old('title') !!}">
    @if($errors->has('title'))
        <div class="alert alert-danger" role="alert">{{ $errors->first('title') }}</div>
    @endif
</div>

<div class="form-group">
    <label for="url">Url</label>
    <input type="text" class="form-control" id="url" name="url" placeholder="Url invoeren" value="{!! $link->url ?? old('url') !!}">
    @if($errors->has('url'))
        <div class="alert alert-danger" role="alert">{{ $errors->first('url') }}</div>
    @endif
</div>
