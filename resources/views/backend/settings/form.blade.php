@csrf

<h3>Website Informatie</h3>
<div class="form-group">
    <label for="title">Titel</label>
    <input type="text" class="form-control" id="title" name="title" placeholder="Titel invoeren" value="{!! $setting->title ?? old('title') !!}">
    @if($errors->has('title'))
        <div class="alert alert-danger" role="alert">{{ $errors->first('title') }}</div>
    @endif
</div>
<div class="form-group">
    <label for="description">Omschrijving</label>
    <input type="text" class="form-control" id="description" name="description" placeholder="Omschrijving invoeren" value="{!! $setting->description ?? old('description') !!}">
    @if($errors->has('description'))
        <div class="alert alert-danger" role="alert">{{ $errors->first('description') }}</div>
    @endif
</div>
<div class="form-group">
    <label for="keywords">Trefwoorden</label>
    <input type="text" class="form-control" id="keywords" name="keywords" placeholder="Trefwoorden invoeren" value="{!! $setting->keywords ?? old('keywords') !!}">
    @if($errors->has('keywords'))
        <div class="alert alert-danger" role="alert">{{ $errors->first('keywords') }}</div>
    @endif
</div>
<div class="form-group">
    <label for="author">Auteur</label>
    <input type="text" class="form-control" id="author" name="author" placeholder="Auteur invoeren" value="{!! $setting->author ?? old('author') !!}">
    @if($errors->has('author'))
        <div class="alert alert-danger" role="alert">{{ $errors->first('author') }}</div>
    @endif
</div>
<hr>

<h3>Adres Informatie</h3>
<div class="form-group">
    <label for="location">Locatie</label>
    <input type="text" class="form-control" id="location" name="location" placeholder="Locatie invoeren" value="{!! $setting->location ?? old('location') !!}">
    @if($errors->has('location'))
        <div class="alert alert-danger" role="alert">{{ $errors->first('location') }}</div>
    @endif
</div>
<div class="form-group">
    <label for="adres">Address</label>
    <input type="text" class="form-control" id="adres" name="adres" placeholder="Address invoeren" value="{!! $setting->adres ?? old('adres') !!}">
    @if($errors->has('adres'))
        <div class="alert alert-danger" role="alert">{{ $errors->first('adres') }}</div>
    @endif
</div>
<div class="form-group">
    <label for="postal_code">Post code</label>
    <input type="text" class="form-control" id="postal_code" name="postal_code" placeholder="Post code invoeren" value="{!! $setting->postal_code ?? old('postal_code') !!}">
    @if($errors->has('postal_code'))
        <div class="alert alert-danger" role="alert">{{ $errors->first('postal_code') }}</div>
    @endif
</div>
<div class="form-group">
    <label for="city">Plaats</label>
    <input type="text" class="form-control" id="city" name="city" placeholder="Plaats invoeren" value="{!! $setting->city ?? old('city') !!}">
    @if($errors->has('city'))
        <div class="alert alert-danger" role="alert">{{ $errors->first('city') }}</div>
    @endif
</div>
<hr>

<h3>Contact informatie</h3>
<div class="form-group">
    <label for="name">Naam</label>
    <input type="text" class="form-control" id="name" name="name" placeholder="Naam invoeren" value="{!! $setting->name ?? old('name') !!}">
    @if($errors->has('name'))
        <div class="alert alert-danger" role="alert">{{ $errors->first('name') }}</div>
    @endif
</div>
<div class="form-group">
    <label for="phone_number">Telefoon nummer</label>
    <input type="text" class="form-control" id="phone_number" name="phone_number" placeholder="Telefoon nummer invoeren" value="{!! $setting->phone_number ?? old('phone_number') !!}">
    @if($errors->has('phone_number'))
        <div class="alert alert-danger" role="alert">{{ $errors->first('phone_number') }}</div>
    @endif
</div>
<div class="form-group">
    <label for="email">Email</label>
    <input type="text" class="form-control" id="email" name="email" placeholder="Email invoeren" value="{!! $setting->email ?? old('email') !!}">
    @if($errors->has('email'))
        <div class="alert alert-danger" role="alert">{{ $errors->first('email') }}</div>
    @endif
</div>











