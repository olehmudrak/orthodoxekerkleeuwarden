@extends('layouts.backend', ['title' => 'Instellingen'])

@section('content')
	<!-- Main content -->
	<div class="content">
		<div class="container mb-3">
		  <div class="row">
        <div class="col-12">
          <button class="btn btn-secondary" onclick="window.history.back()">Terug</button>
        </div>
		  </div>
    </div>
      <div class="container">
          <form action="{{ route('backend.settings.update', ['setting' => $setting]) }}" method="post">
            @method('PUT')
            @include('backend.settings.form')

            <button type="submit" class="btn btn-primary float-right mb-5">aanpassen</button>
          </form>
      </div>
	  </div>
	</div>
@endsection



