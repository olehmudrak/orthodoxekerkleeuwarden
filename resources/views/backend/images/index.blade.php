@extends('layouts.backend', ['title' => 'Afbeeldingen'])

@section('content')
	<!-- Main content -->
	<div class="content">
		<div class="container-fluid mb-3">
		  <div class="row justify-content-end">
			<a href="{{ route('backend.images.create') }}"><button class="btn btn-success float-right">Toevoegen</button></a>
		  </div>
		</div>
		<div class="table-responsive-sm">
			<table id="imageTable" class="table table-hover">
			<thead>
				<tr>
                    <th scope="col">#</th>
                    <th scope="col">Thumbnail</th>
					<th scope="col">Titel</th>
					<th scope="col">Categorie</th>
					<th scope="col">Acties</th>
				</tr>
			</thead>
			<tfoot>
				<tr>
                    <th scope="col">#</th>
                    <th scope="col">Thumbnail</th>
					<th scope="col">Titel</th>
					<th scope="col">Categorie</th>
					<th scope="col">Acties</th>
				</tr>
			</tfoot>
			</table>
		</div>
	  </div>
	</div>
@endsection

@section('scripts')
<script src="{{ asset('js/datatables.js') }}" defer></script>
@endsection

