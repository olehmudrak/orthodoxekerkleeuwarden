@csrf

<div class="form-group">
    <label for="title">Titel</label>
    <input type="text" class="form-control" id="title" name="title" placeholder="Titel invoeren" value="{!! $image->title ?? old('title') !!}">
    @if($errors->has('title'))
        <div class="alert alert-danger" role="alert">{{ $errors->first('title') }}</div>
    @endif
</div>

<div class="form-group">
    <label for="category">Category</label>
    <select name="category_id" id="category" class="form-control" value="{!! $image->category->id ?? old('category_id') !!}">
        <option value="0">Geen categorie</option>
        @foreach ($categories as $category)
            <option value="{{ $category->id }}" {{ (old('category_id') == $category->id || (isset($image->category->id) && $image->category->id == $category->id) ? "selected":"") }}>{{ $category->name }}</option>
        @endforeach
    </select>
    @if($errors->has('category_id'))
        <div class="alert alert-danger" role="alert">{{ $errors->first('category_id') }}</div>
    @endif
</div>

<div class="form-group">
    <label for="image">Afbeelding</label>
    <input type="file" id="image" name="{{ empty($image->image) ? 'image[]':'image' }}" {{ empty($image->image) ? "multiple":"" }}>
    @if($errors->has('image'))
    <div class="alert alert-danger" role="alert">{{ $errors->first('image') }}</div>
@endif
</div>

@if (!empty($image->image))
<h4>Huidige Afbeelding</h4>
<a href="{{ asset('storage/images/' . $image->image) }}">
    <img src="{{ asset('storage/images/' . $image->image) }}" alt="Afbeelding" class="mx-auto d-block preview-image">
</a>
@endif