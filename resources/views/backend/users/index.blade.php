@extends('layouts.backend', ['title' => 'Gebruikers'])

@section('content')
	<!-- Main content -->
	<div class="content">
		<div class="container-fluid mb-3">
		  <div class="row justify-content-end">
			<a href="{{ route('backend.users.create') }}"><button class="btn btn-success float-right">Toevoegen</button></a>
		  </div>
		</div>
		<div class="table-responsive-sm">
			<table class="table table-hover">
			<thead>
				<tr>
					<th scope="col">#</th>
					<th scope="col">Naam</th>
					<th scope="col">Email</th>
					<th scope="col">Rol</th>
					<th scope="col">Gemaakt op</th>
					<th colspan="2" scope="col">Acties</th>
				</tr>
			</thead>
			<tbody>
				@foreach ($users as $user)
					<tr>
						<th scope="row">{{ $user->id }}</th>
						<td>{{ $user->name }}</td>
						<td>{{ $user->email }}</td>
						<td>{{ ucfirst($user->getRoleNames()[0]) }}</td>
						<td>{{ $user->created_at }}</td>
						<td>
							<a href="{{ route('backend.users.edit', ['user' => $user]) }}">
								<button class="btn btn-primary"><i class="fas fa-edit mr-1"></i>Aanpassen</button>
							</a>
						</td>
						<td>
						<form action="{{ route('backend.users.destroy', ['user' => $user]) }}" method="POST">
							@method('delete')
							@csrf
							<button type="submit" class="btn btn-danger"><i class="fas fa-trash-alt mr-1"></i>Verwijderen</button>
						</form>
						</td>
					</tr>
				@endforeach
			</tbody>
			</table>
		</div>
		<div class="container">
			{{ $users->links() }}
		</div>
	  </div>
	</div>
@endsection

