@extends('layouts.backend', ['title' => 'Gebruiker Aanpassen'])

@section('content')
	<!-- Main content -->
	<div class="content">
		<div class="container mb-3">
		  <div class="row">
        <div class="col-6">
          <button class="btn btn-secondary" onclick="window.history.back()">Terug</button>
        </div>
        <div class="col-6">
          <form action="{{ route('backend.users.destroy', ['user' => $user]) }}" method="POST">
            @method('delete')
            @csrf
            <button type="submit" class="btn btn-danger float-right"><i class="fas fa-trash-alt mr-1"></i>Verwijderen</button>
          </form> 
        </div>
		  </div>
    </div>
      <div class="container">
          <form action="{{ route('backend.users.update', ['user' => $user]) }}" method="post">
              @method('PUT')
              @include('backend.users.form')
              
              <button type="submit" class="btn btn-primary float-right">Aanpassaen</button>
          </form>
      </div>
	  </div>
	</div>
@endsection

