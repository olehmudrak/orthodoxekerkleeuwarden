@csrf

<div class="form-group">
    <label for="name">Naam</label>
    <input type="text" class="form-control" id="name" name="name" placeholder="Naam invoeren" value="{!! $user->name ?? old('name') !!}">
    @if($errors->has('name'))
        <div class="alert alert-danger" role="alert">{{ $errors->first('name') }}</div>
    @endif
</div>

<div class="form-group">
    <label for="email">Email</label>
    <input type="email" class="form-control" id="email" name="email" placeholder="Email invoeren" value="{!! $user->email ?? old('email') !!}">
    @if($errors->has('email'))
        <div class="alert alert-danger" role="alert">{{ $errors->first('email') }}</div>
    @endif
</div>

@if(auth()->user()->hasRole('admin'))
    <div class="form-group">
        <label for="role">Rol</label>
        <select name="role" id="role" class="form-control" value="{!! $user->getRoleNames()[0] ?? old('role') !!}">
            @foreach ($roles as $role)
                <option value="{{ $role->name }}" {{ (old("role") == $role->name || (isset($user->getRoleNames()[0]) && $user->getRoleNames()[0] == $role->name) ? "selected":"") }}>{{ ucfirst($role->name) }}</option>
            @endforeach
        </select>
        @if($errors->has('role'))
            <div class="alert alert-danger" role="alert">{{ $errors->first('role') }}</div>
        @endif
    </div>
@endif

<div class="form-group">
    <label for="password">Wachtwoord</label>
    <input type="password" class="form-control" id="password" name="password" placeholder="Wachtwoord invoeren">
    @if($errors->has('password'))
        <div class="alert alert-danger" role="alert">{{ $errors->first('password') }}</div>
    @endif
</div>

<div class="form-group">
    <label for="password_confirmation">Wachwoord bevestiging</label>
    <input type="password" class="form-control" id="password_confirmation" name="password_confirmation" placeholder="Wachtwoord bevestiging invoeren">
</div>