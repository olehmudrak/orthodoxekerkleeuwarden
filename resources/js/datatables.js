$(function () {
    $('#imageTable').DataTable( {
        "language": {
            "url": "//cdn.datatables.net/plug-ins/1.10.21/i18n/Dutch.json"
        },
        processing: true,
        serverSide: true,
        ajax: {
            url: '/backend/images'
        },
        columns: [
            {
                data:'id',
                name:'#'
            },
            {
                data:'thumbnail',
                name:'thumbnail',
                orderable: false
            },
            {
                data:'title',
                name:'title'
            },
            {
                data:'category',
                name:'category'
            },
            {
                data:'action',
                name:'action',
                orderable: false
            }
        ],
    } );
} );