<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

class Service extends Model
{
    protected $guarded = [];

    public function getDateAttribute($date)
    {
        setlocale(LC_TIME, 'nl');
        return Carbon::parse($date, 'Europe/Amsterdam')->formatLocalized('%d %B %Y');
    }

    public function type()
    {
        return $this->belongsTo('App\ServiceType', 'service_type_id');
    }
}
