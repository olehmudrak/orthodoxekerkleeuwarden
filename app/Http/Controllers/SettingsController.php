<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Setting;

class SettingsController extends Controller
{
    public function edit(Setting $setting)
    {
        // $setting = Setting::findOrFail($setting->id);
        return view('backend.settings.edit');
    }

    public function update(Setting $setting)
    {
        $data = request()->validate([
            'title' => 'required',
            'description' => 'required',
            'keywords' => 'required',
            'author' => 'required',
            'location' => 'required',
            'adres' => 'required',
            'postal_code' => 'required',
            'city' => 'required',
            'name' => 'required',
            'phone_number' => 'required',
            'email' => 'required',
        ]);
        $setting->update($data);
        return redirect('/backend/settings/' . $setting->id . '/edit')->with('success', 'Instellingen zijn aangepast');
    }
}
