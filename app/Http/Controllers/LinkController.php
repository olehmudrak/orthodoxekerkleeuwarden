<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Link;

class LinkController extends Controller
{
    public function index()
    {
        $links = Link::paginate(10);
        return view('backend.links.index', compact('links'));
    }

    public function create()
    {
        $link = new Link();
        return view('backend.links.create', compact('link'));
    }

    public function store()
    {
        $data = request()->validate($this->validation());
        $link = Link::create($data);
        return redirect('/backend/links')->with('success', 'Link ' . $link->title . ' is toegevoegd');
    }

    public function edit(Link $link)
    {
        return view('backend.links.edit', compact('link'));
    }

    public function update(Link $link)
    {
        $data = request()->validate($this->validation());
        $link->update($data);
        return redirect('/backend/links')->with('success', 'Link ' . $link->title . ' is aangepast');
    }

    public function destroy(Link $link)
    {
        $title = $link->title;
        $link->delete();
        return redirect('/backend/links')->with('success', 'Link ' . $title . ' is verwijderd');
    }

    private function validation(): array
    {
        return [
            'title' => 'required',
            'url'   => 'required|url'
        ];
    }
}
