<?php

namespace App\Http\Controllers;

use App\User;
use Spatie\Permission\Models\Role;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Exceptions\UnauthorizedException;

class UserController extends Controller
{
    public function index()
    {
        $users = User::paginate(10);
        return view('backend.users.index', compact('users'));
    }

    public function create()
    {
        $user = new User();
        $roles = Role::all()->sortByDesc('id');
        return view('backend.users.create', compact('user', 'roles'));
    }

    public function store()
    {
        $data = request()->validate($this->validation());
        $data['password'] = Hash::make($data['password']);
        $user = User::create($data);
        $user->assignRole($data['role']);

        return redirect('/backend/users')->with('success', 'Gebruiker ' . $user->name . ' is toegevoegd');
    }

    public function edit(User $user)
    {
        if (!auth()->user()->hasRole('admin') && auth()->user()->id !== $user->id) {
            throw UnauthorizedException::forRoles(['admin']);
        }
        $roles = !auth()->user()->hasRole('admin') ? Role::where('name', '!=', 'admin')->get() : Role::all();
        return view('backend.users.edit', compact('user', 'roles'));
    }

    public function update(User $user)
    {
        $admin = auth()->user()->hasRole('admin');
        if (!$admin && auth()->user()->id !== $user->id) {
            throw UnauthorizedException::forRoles(['admin']);
        }
        $validation = $this->validation();
        if (!request()->input('password')) {
            unset($validation['password']);
        }
        if (request()->input('email') === $user->email) {
            unset($validation['email']);
        }
        $data = request()->validate($validation);
        if (isset($data['password'])) {
            $data['password'] = Hash::make($data['password']);
        }
        $user->update($data);
        if ($admin) {
            $user->syncRoles($data['role']);
        }

        $redirectLink = auth()->user()->hasRole('admin') && $data['role'] === 'admin' ? '/backend/users' : '/backend/users/' . $user->id . '/edit';

        return redirect($redirectLink)->with('success', 'Gebruiker ' . $user->name . ' is aangepast');
    }

    public function destroy(User $user)
    {
        if (!auth()->user()->hasRole('admin') && auth()->user()->id !== $user->id) {
            throw UnauthorizedException::forRoles(['admin']);
        }
        $username = $user->name;
        $user->delete();
        return redirect('/backend/users')->with('success', 'Gebruiker ' . $username . ' is verwijderd');
    }

    private function validation(): array
    {
        return [
            'name' => 'required',
            'email' => 'email|required|unique:users',
            'password' => 'required|confirmed|min:6',
            'role' => ''
        ];
    }
}
