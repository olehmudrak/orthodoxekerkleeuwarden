<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Page;
use App\Category;

class GalleryController extends Controller
{
    public function index()
    {
        $page = Page::findOrFail(4);
        $categories = Category::all();
        return view('gallery.index', compact('page', 'categories'));
    }

    public function show($id)
    {
        $category = Category::findOrFail($id);
        $images = [];
        foreach ($category->images as $image) {
            $images[] = asset('storage/images/' . $image->image);
        }
        return view('gallery.show', compact('category', 'images'));
    }
}
