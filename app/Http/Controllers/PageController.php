<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Page;
use App\Service;
use App\ServiceType;
use App\Link;
use App\Mail\ContactMail;
use App\Setting;
use Illuminate\Support\Facades\Mail;

class PageController extends Controller
{
	public function index()
	{
		$page = Page::findOrFail(1);
		$gallery = isset($page->category) && count($page->category->images) > 0 ? $page->category->images->sortByDesc('created_at')->values() : false;
		$services = Service::whereDate('date', '>=', now())->take(4)->get();
		$types = ServiceType::all();
		$content = json_decode($page->content);

		return view('pages.index', compact(['page', 'gallery', 'content', 'services', 'types']));
	}

	public function services()
	{
		$page = Page::findOrFail(2);
		$services = Service::paginate(10);
		$types = ServiceType::all();

		return view('pages.services', compact('page', 'services', 'types'));
	}

	public function request()
	{
		$page = Page::findOrFail(3);
		$content = json_decode($page->content);

		return view('pages.request', compact(['page', 'content']));
	}

	public function contact()
	{
		$page = Page::findOrFail(5);

		return view('pages.contact', compact('page'));
	}

	public function sendEmail()
	{
		$data = request()->validate([
			'name' => 'required',
			'email' => 'required|email',
			'subject' => 'required',
			'content' => 'required'
		]);

		$setting = Setting::first();

		Mail::to($setting->email)->send(new ContactMail([
			'name' => $data['name'],
			'email' => $data['email'],
			'subject' => $data['subject'],
			'content' => $data['content'],
		]));

		return redirect('/contact')->with('success', 'Email is succesvol verzonden');
	}

	public function links()
	{
		$page = Page::findOrFail(6);
		$links = Link::all();

		return view('pages.links', compact('page', 'links'));
	}

	public function about()
	{
		$page = Page::findOrFail(7);
		$content = json_decode($page->content);

		return view('pages.about', compact(['page', 'content']));
	}
}
