<?php

namespace App\Http\Controllers;

use App\Image as ImageModel;
use App\Category;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;

class ImageController extends Controller
{
	public function index(Request $request)
	{
		if ($request->ajax()) {
			$data = ImageModel::latest()->get();

			return datatables()->of($data)
				->addColumn('thumbnail', function ($data) {
					return '<a href="' . asset('storage/images/' . $data->image) . '">
                        <img src="' . asset('storage/images/' . $data->image) . '" alt="Afbeelding" class="mx-auto d-block preview-image">
                    </a>';
				})
				->addColumn('category', function ($data) {
					return '<td>' . isset($data->category) && isset($data->category->name) ? $data->category->name : '' . '</td>';
				})
				->addColumn('action', function ($data) {
					$button = '<td>
                    <a href="' . route('backend.images.edit', ['image' => $data->id]) . '">
                        <button class="btn btn-primary"><i class="fas fa-edit mr-1"></i>Aanpassen</button>
                        </a>
                    </td>';
					$button .= '<form action="' . route('backend.images.destroy', ['image' => $data->id]) . '" method="POST">
                    <input type="hidden" name="_method" value="delete">
                    ' . csrf_field() . '
                    <button type="submit" class="btn btn-danger"><i class="fas fa-trash-alt mr-1"></i>Verwijderen</button>
                    </form>';

					return $button;
				})
				->rawColumns(['thumbnail', 'category', 'action'])
				->toJson();
		}

		return view('backend.images.index');
	}

	public function create()
	{
		$image = new ImageModel();
		$categories = Category::all();

		return view('backend.images.create', compact(['image', 'categories']));
	}

	public function store()
	{
		$data = request()->validate($this->validation());
		foreach (request()->file('image') as $image) {
			$data['image'] = $this->uploadImage('', $image);
			ImageModel::create($data);
		}

		return redirect('/backend/images')->with('success', 'Afbeelding[en] is/zijn toegevoegd');
	}

	public function edit(ImageModel $image)
	{
		$categories = Category::all();

		return view('backend.images.edit', compact(['image', 'categories']));
	}

	public function update(ImageModel $image)
	{
		$validation = $this->validation();
		if ($image->image) {
			unset($validation['image']);
		}
		$data = request()->validate($validation);
		$data['image'] = $this->uploadImage($image->image, request()->file('image'));
		$image->update($data);

		return redirect('/backend/images')->with('success', 'Afbeelding ' . $image->image . ' is aangepast');
	}

	public function destroy(ImageModel $image)
	{
		$imageName = $image->image;
		$image->delete();
		Storage::delete('/public/images/' . $imageName);

		return redirect('/backend/images')->with('success', 'Afbeelding ' . $imageName . ' is verwijderd');
	}

	private function validation(): array
	{
		return [
			'category_id' => '',
			'title' => '',
			'image' => 'required',
			'image.*' => 'image|mimes:jpeg,png,jpg,gif,svg'
		];
	}

	public function uploadImage($old, $new): string
	{
		$folder = 'public/images';

		if ($new) {
			if (!empty($old)) {
				Storage::delete('/' . $folder . '/' . $old);
			}

			$extension = $new->getClientOriginalExtension();
			$fileNameToStore = Str::random(40) . '.' . $extension;
			$width = 1280;
			$height = 1280;
			$image = Image::make($new)->orientate();
			if ($image->height() < $height) {
				$height = $image->height();
			}
			if ($image->width() < $width) {
				$width = $image->width();
			}
			$image->height() > $image->width() ? $width = null : $height = null;
			$resize = $image->resize($width, $height, function ($constraint) {
				$constraint->aspectRatio();
			})->encode('jpg');
			Storage::put("public/images/{$fileNameToStore}", $resize->__toString());

			return $fileNameToStore;
		}

		return $old;
	}
}
