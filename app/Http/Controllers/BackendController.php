<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use App\Page;
use App\Category;
use Intervention\Image\Facades\Image;

class BackendController extends Controller
{
    public function index()
    {
        return view('backend.index');
    }

    public function home()
    {
        $page = Page::findOrFail(1);
        $categories = Category::all();
        $content = json_decode($page->content);
        return view('backend.pages.home', compact(['page', 'categories','content']));
    }

    public function services()
    {
        $page = Page::findOrFail(2);
        return view('backend.pages.services', compact('page'));
    }

    public function request()
    {
        $page = Page::findOrFail(3);
        $categories = Category::all();
        $content = json_decode($page->content);
        return view('backend.pages.request', compact(['page','categories','content']));
    }

    public function gallery()
    {
        $page = Page::findOrFail(4);
        return view('backend.pages.gallery', compact('page'));
    }

    public function contact()
    {
        $page = Page::findOrFail(5);
        return view('backend.pages.contact', compact('page'));
    }

    public function links()
    {
        $page = Page::findOrFail(6);
        return view('backend.pages.links', compact('page'));
    }

    public function about()
    {
        $page = Page::findOrFail(7);
        $content = json_decode($page->content);
        return view('backend.pages.about', compact(['page','content']));
    }

    public function update(Page $page)
    {
        $data = request()->validate([
            'title' => 'required',
            'subtitle' => '',
            'category_id' => '',
            'banner' => 'mimes:jpeg,png,jpg,gif,svg',
            'column-title' => '',
            'content' => '',
            'column-image' => '',
        ]);
        
        $page->title = $data['title'];
        $page->subtitle = $data['subtitle'] ?? '';
        $page->category_id = $data['category_id'] ?? 0;

        $page->banner = $this->uploadImage($page->banner, request()->file('banner'));

        $page->content = $this->generatePageContent($page->id, $data, json_decode($page->content));

        $page->update();

        return redirect(url()->previous())->with('success', 'Pagina is aangepast');
    }

    private function generatePageContent($id, $data, $oldContent)
    {
        $modifiedContent = [];
        switch ($id) {
            case 1:
            case 3:
            case 7:
                for ($i = 0; $i < count($data['content']); $i++) {
                    $modifiedContent[$i]['title'] = $data['column-title'][$i] ?? '';
                    $modifiedContent[$i]['content'] = $data['content'][$i] ?? '';
                    $oldImage = isset($oldContent[$i]->image) ? $oldContent[$i]->image : '';
                    $newImage = isset(request()->file('column-image')[$i]) ? request()->file('column-image')[$i] : '';
                    $modifiedContent[$i]['image'] = $this->uploadImage($oldImage, $newImage);
                }
                break;
            default:
                return '';
        }

        return json_encode(($modifiedContent));
    }

    public function uploadImage($old, $new)
    {
        $folder = '/public/images';

        if ($new) {
            if (!empty($old) && $old !== 'empty.jpg') {
                Storage::delete('/' . $folder . '/' . $old);
            }

            $extension = $new->getClientOriginalExtension();
            $fileNameToStore = Str::random(40) . '.' . $extension;
            $width = 1920;
            $height = 1920;
            $image = Image::make($new)->orientate();
            if ($image->height() < $height) {
                $height = $image->height();
            }
            if ($image->width() < $width) {
                $width = $image->width();
            }
            $image->height() > $image->width() ? $width=null : $height=null;
            $resize = $image->resize($width, $height, function ($constraint) {
              $constraint->aspectRatio();
            })->encode($extension);
            Storage::put("public/images/{$fileNameToStore}", $resize->__toString());

            return $fileNameToStore;
        }

        return $old;
    }
}
