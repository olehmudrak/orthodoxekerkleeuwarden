<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    public function index()
    {
        $categories = Category::paginate(10);
        return view('backend.categories.index', compact('categories'));
    }

    public function create()
    {
        $category = new Category();
        return view('backend.categories.create', compact('category'));
    }

    public function store()
    {
        $data = request()->validate($this->validation());
        $category = Category::create($data);
        return redirect('/backend/categories')->with('success', 'Categorie ' . $category->name . ' is toegevoegd');
    }

    public function edit(Category $category)
    {
        return view('backend.categories.edit', compact('category'));
    }

    public function update(Category $category)
    {
        $data = request()->validate($this->validation());
        $category->update($data);
        return redirect('/backend/categories')->with('success', 'Categorie ' . $category->name . ' is aangepast');
    }

    public function destroy(Category $category)
    {
        $name = $category->name;
        $category->delete();
        return redirect('/backend/categories')->with('success', 'Categorie ' . $name . ' is verwijderd');
    }

    private function validation(): array
    {
        return [
            'name' => 'required',
        ];
    }
}
