<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Service;
use App\ServiceType;

class ServiceController extends Controller
{
    public function index()
    {
        $services = Service::paginate(10);
        return view('backend.services.index', compact('services'));
    }

    public function create()
    {
        $service = new Service();
        $types = ServiceType::all();
        return view('backend.services.create', compact('service', 'types'));
    }

    public function store()
    {
        Service::create(request()->validate($this->validation()));
        return redirect('/backend/services')->with('success', 'Dienst is toegevoegd');
    }

    public function edit(Service $service)
    {
        $type = Service::findOrFail($service->id);
        $types = ServiceType::all();
        return view('backend.services.edit', compact('service', 'types'));
    }

    public function update(Service $service)
    {
        $service->update(request()->validate($this->validation()));
        return redirect('/backend/services')->with('success', 'Dienst is aangepast');
    }

    public function destroy(Service $service)
    {
        $service->delete();
        return redirect('/backend/services')->with('success', 'Dienst  is verwijderd');;
    }

    private function validation(): array
    {
        return [
            'date' => 'required',
            'service_type_id' => 'required',
            'description' => '',
            'color' => 'string',
        ];
    }
}
