<?php

namespace App\Http\Controllers;

use App\Service;
use Illuminate\Http\Request;
use App\ServiceType;

class ServiceTypeController extends Controller
{
    public function index()
    {
        $types = ServiceType::paginate(10);
        return view('backend.services.types.index', compact('types'));
    }

    public function create()
    {
        $type = new ServiceType();
        return view('backend.services.types.create', compact('type'));
    }

    public function store()
    {
        $data = request()->validate($this->validation());
        $serviceType = ServiceType::create($data);
        return redirect('/backend/services/types')->with('success', 'Dienst type ' . $serviceType->name . ' is toegevoegd');
    }

    public function edit(ServiceType $type)
    {
        $type = ServiceType::findOrFail($type->id);
        return view('backend.services.types.edit', compact('type'));
    }

    public function update(ServiceType $type)
    {
        $validation = $this->validation();
        if (request()->input('name') === $type->name) {
            unset($validation['name']);
        }
        $data = request()->validate($validation);

        $type->update($data);

        return redirect('/backend/services/types')->with('success', 'Dienst type ' . $type->name . ' is aangepast');
    }

    public function destroy(ServiceType $type)
    {
        $name = $type->name;
        $type->services()->delete();
        $type->delete();
        return redirect('/backend/services/types')->with('success', 'Dienst type ' . $name . ' is verwijderd');
        ;
    }

    private function validation(): array
    {
        return [
            'name' => 'string|required|unique:service_types',
            'time' => 'required',
        ];
    }
}
