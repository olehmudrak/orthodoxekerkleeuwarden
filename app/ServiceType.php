<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ServiceType extends Model
{
    protected $guarded = [];

    public function getTimeAttribute($time)
    {
        return substr($time, 0, 5);
    }

    public function services()
    {
        return $this->hasMany('App\Service');
    }
}
