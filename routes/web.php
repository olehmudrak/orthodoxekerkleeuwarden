<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Mail\ContactMail;

Route::get('/', 'PageController@index')->name('pages.index');
Route::get('/services', 'PageController@services')->name('pages.services');
Route::get('/request', 'PageController@request')->name('pages.request');
Route::get('/contact', 'PageController@contact')->name('pages.contact');
Route::get('/links', 'PageController@links')->name('pages.links');
Route::get('/about', 'PageController@about')->name('pages.about');

Route::get('/gallery', 'GalleryController@index')->name('gallery.index');
Route::get('/gallery/{id}', 'GalleryController@show')->name('gallery.show');

Route::post('/mail/contact', 'PageController@sendEmail')->name('mail.contact');

Auth::routes(['register' => false]);
Route::prefix('backend')->middleware(['auth'])->group(function () {
    Route::get('/dashboard', 'BackendController@index')->name('backend.dashboard');

    // Pages
    Route::get('/pages/home', 'BackendController@home')->name('backend.pages.home');
    Route::get('/pages/services', 'BackendController@services')->name('backend.pages.services');
    Route::get('/pages/request', 'BackendController@request')->name('backend.pages.request');
    Route::get('/pages/gallery', 'BackendController@gallery')->name('backend.pages.gallery');
    Route::get('/pages/contact', 'BackendController@contact')->name('backend.pages.contact');
    Route::get('/pages/links', 'BackendController@links')->name('backend.pages.links');
    Route::get('/pages/about', 'BackendController@about')->name('backend.pages.about');
    Route::post('/pages/{page}', 'BackendController@update')->name('backend.pages.update');

    // Services
    Route::get('/services', 'ServiceController@index')->name('backend.services.index');
    Route::get('/services/create', 'ServiceController@create')->name('backend.services.create');
    Route::post('/services', 'ServiceController@store')->name('backend.services.store');
    Route::get('/services/{service}/edit', 'ServiceController@edit')->name('backend.services.edit');
    Route::put('/services/{service}/', 'ServiceController@update')->name('backend.services.update');
    Route::delete('/services/{service}/', 'ServiceController@destroy')->name('backend.services.destroy');

    // Service Types
    Route::get('/services/types', 'ServiceTypeController@index')->name('backend.service.types.index');
    Route::get('/services/types/create', 'ServiceTypeController@create')->name('backend.service.types.create');
    Route::post('/services/types', 'ServiceTypeController@store')->name('backend.service.types.store');
    Route::get('/services/types/{type}/edit', 'ServiceTypeController@edit')->name('backend.service.types.edit');
    Route::put('/services/types/{type}/', 'ServiceTypeController@update')->name('backend.service.types.update');
    Route::delete('/services/types/{type}/', 'ServiceTypeController@destroy')->name('backend.service.types.destroy');

    // Users
    Route::group(['middleware' => ['role:admin']], function () {
        Route::get('/users', 'UserController@index')->name('backend.users.index');
        Route::get('/users/create', 'UserController@create')->name('backend.users.create');
        Route::post('/users', 'UserController@store')->name('backend.users.store');
    });
    Route::get('/users/{user}/edit', 'UserController@edit')->name('backend.users.edit');
    Route::put('/users/{user}/', 'UserController@update')->name('backend.users.update');
    Route::delete('/users/{user}/', 'UserController@destroy')->name('backend.users.destroy');

    // User Account
    Route::get('/user/{user}/edit', 'UserController@edit')->name('backend.user.edit');

    // Links
    Route::get('/links', 'LinkController@index')->name('backend.links.index');
    Route::get('/links/create', 'LinkController@create')->name('backend.links.create');
    Route::post('/links', 'LinkController@store')->name('backend.links.store');
    Route::get('/links/{link}/edit', 'LinkController@edit')->name('backend.links.edit');
    Route::put('/links/{link}/', 'LinkController@update')->name('backend.links.update');
    Route::delete('/links/{link}/', 'LinkController@destroy')->name('backend.links.destroy');

    // Images
    Route::get('/images', 'ImageController@index')->name('backend.images.index');
    Route::get('/images/create', 'ImageController@create')->name('backend.images.create');
    Route::post('/images', 'ImageController@store')->name('backend.images.store');
    Route::get('/images/{image}/edit', 'ImageController@edit')->name('backend.images.edit');
    Route::put('/images/{image}/', 'ImageController@update')->name('backend.images.update');
    Route::delete('/images/{image}/', 'ImageController@destroy')->name('backend.images.destroy');

    // Categories
    Route::get('/categories', 'CategoryController@index')->name('backend.categories.index');
    Route::get('/categories/create', 'CategoryController@create')->name('backend.categories.create');
    Route::post('/categories', 'CategoryController@store')->name('backend.categories.store');
    Route::get('/categories/{category}/edit', 'CategoryController@edit')->name('backend.categories.edit');
    Route::put('/categories/{category}/', 'CategoryController@update')->name('backend.categories.update');
    Route::delete('/categories/{category}/', 'CategoryController@destroy')->name('backend.categories.destroy');

    Route::group(['middleware' => ['role:admin']], function () {
        // Settings
        Route::get('/settings/{setting}/edit', 'SettingsController@edit')->name('backend.settings.edit');
        Route::put('/settings/{setting}/', 'SettingsController@update')->name('backend.settings.update');
    });
});

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/email', function() {
    return new ContactMail();
});
